package edu.uky.rc.uklibrary;

import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.ion.NullValueException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Scanner;

import static net.sourceforge.argparse4j.impl.Arguments.storeTrue;

public class Wrapper {
    private static final Logger logger = LoggerFactory.getLogger(Wrapper.class);

    public static void main(String[] args) throws IOException {
        ArgumentParser parser = ArgumentParsers.newArgumentParser("Wrapper Example")
                .defaultHelp(true)
                .description("Access to S3-Compatible Object Storage systems.");
        parser.addArgument("-a", "--action")
                .choices("create", "list", "pack", "compress", "upload", "download", "unpack", "decompress", "delete",
                        "test")
                .setDefault("list")
                .help("Program mode of operation");
        parser.addArgument("-b", "--bagit")
                .choices("none", "dotfile", "standard").setDefault("none")
                .help("Write to a BagIt (v1.0) bag.");
        parser.addArgument("-c", "--compress")
                .choices("none", "tar", "bzip2", "gzip", "xz", "zip").setDefault("none")
                .help("Compression method to use on upload.");
        parser.addArgument("-ha", "--hash")
                .choices("md5", "sha1", "sha256", "sha512").setDefault("md5")
                .help("Hashing method to use in BagIt bag.");
        parser.addArgument("-hf", "--hiddenfiles").action(storeTrue())
                .help("Include hidden files in BagIt container.");
        parser.addArgument("-s", "--save").action(storeTrue())
                .help("Save the generated compressed file.");
        parser.addArgument("-n", "--numconns").type(Integer.class).setDefault(50)
                .help("The number of max connections to use for a transfer.");
        parser.addArgument("-r", "--remove").action(storeTrue())
                .help("Remove existing object(s) on upload");
        /*parser.addArgument("-x", "--extract").action(storeTrue())
                .help("Attempt to extract downloaded file.");*/
        parser.addArgument("-C", "--credentials")
                .setDefault("config.properties")
                .help("Configuration file holding credential information.");
        parser.addArgument("-A", "--accesskey");
        parser.addArgument("-S", "--secretkey");
        parser.addArgument("-E", "--endpoint");
        parser.addArgument("-R", "--region");
        parser.addArgument("directories").nargs("*");
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        int maxConnections = ns.getInt("numconns");
        boolean /*extract = ns.getBoolean("extract"), *//*hiddenFiles = ns.getBoolean("hiddenfiles")*/hiddenFiles = true,
                save = ns.getBoolean("save"), remove = ns.getBoolean("remove");
        String action = ns.getString("action"), credentials = ns.getString("credentials"),
                accessKey = ns.getString("accesskey"), secretKey = ns.getString("secretkey"),
                endpoint = ns.getString("endpoint"), region = ns.getString("region"),
                bagit = ns.getString("bagit"), compression = ns.getString("compress"),
                hashing = ns.getString("hash");
        List<String> directories = ns.getList("directories");

        /*try {
            logger.debug("ns args: {}", ns.toString());
            if (directories.size() < 1) {
                logger.error("You must supply a directory to encapsulate.");
                return;
            }
            String dst = Encapsulation.encapsulate(directories.get(0), bagit, hashing, hiddenFiles, compression);
            logger.debug("Encapsulation dst: {}", dst);
            return;
        } catch (Exception e) {
            logger.error("Execution encountered: {}", e.getMessage());
        }*/

        ObjectStorage os;
        logger.trace("Checking for config.properties file");
        File configFile = new File(credentials);
        if (configFile.exists()) {
            Configurations configs = new Configurations();
            try {
                Configuration config = configs.properties(configFile);
                accessKey = config.getString("access.key");
                secretKey = config.getString("secret.key");
                endpoint = config.getString("endpoint");
                region = config.getString("region");
            } catch (ConfigurationException e) {
                logger.error("Error with your config.properties file.");
            }
        }
        if (region != null)
            try {
                os = new ObjectStorage(accessKey, secretKey, endpoint, maxConnections, region);
            } catch (NullValueException e) {
                logger.error("Error building S3 connection : {}", e.getMessage());
                return;
            }
        else
            try {
                os = new ObjectStorage(accessKey, secretKey, endpoint, maxConnections);
            } catch (NullValueException e) {
                logger.error("Error building S3 connection : {}", e.getMessage());
                return;
            }
        switch (action) {
            case "create":
                if (directories.size() > 0 && directories.get(0) != null) {
                    logger.info("Creating bucket: {}", directories.get(0));
                    if (!os.doesBucketExist(directories.get(0))) {
                        if (os.createBucket(directories.get(0)))
                            logger.info("Bucket created successfully.");
                        else
                            logger.error("Failed to create bucket.");
                    } else
                        logger.error("Bucket already exists.");
                } else
                    logger.error("You must supply a bucket to create.");
                break;
            case "list":
                if (directories.size() > 0 && directories.get(0) != null) {
                    if (os.doesBucketExist(directories.get(0))) {
                        List<S3ObjectSummary> summaries = os.bucketContents(directories.get(0));
                        if (summaries.size() > 0) {
                            logger.info("Bucket [{}] objects:", directories.get(0));
                            int longKey = 3, longSize = 8, longMod = 13;
                            for (S3ObjectSummary summary : summaries) {
                                if (summary.getKey().length() > longKey)
                                    longKey = summary.getKey().length();
                                if (Long.toString(summary.getSize()).length() > longSize)
                                    longSize = Long.toString(summary.getSize()).length();
                                if (summary.getLastModified().toString().length() > longMod)
                                    longMod = summary.getLastModified().toString().length();
                            }
                            logger.info(String.format("%-" + (longKey + 1) + "s %-" + (longSize + 1) + "s %-" + longMod + "s",
                                    "Key", "Size (B)", "Last Modified"));
                            logger.info(String.format("%-" + longKey + "s", " ").replace(' ', '-') + "  " +
                                            String.format("%-" + longSize + "s", " ").replace(' ', '-') + "  " +
                                            String.format("%-" + longMod + "s", " ").replace(' ', '-'));
                            for (S3ObjectSummary summary : summaries)
                                logger.info(String.format("%-" + (longKey + 1) + "s %-" + (longSize + 1) + "s %-" + longMod + "s",
                                        summary.getKey(), Long.toString(summary.getSize()), summary.getLastModified().toString()));
                        } else
                            logger.info("Bucket [{}] is empty.", directories.get(0));
                    } else
                        logger.error("Bucket [{}] does not exist.", directories.get(0));
                } else {
                    List<Bucket> buckets = os.buckets();
                    if (buckets.size() > 0) {
                        logger.info("Listing buckets:");
                        int longName = 4, longOwner = 5, longCreated = 7;
                        for (Bucket bucket : buckets) {
                            if (bucket.getName().length() > longName)
                                longName = bucket.getName().length();
                            if (bucket.getOwner().getDisplayName().length() > longOwner)
                                longOwner = bucket.getOwner().getDisplayName().length();
                            if (bucket.getCreationDate().toString().length() > longCreated)
                                longCreated = bucket.getCreationDate().toString().length();
                        }
                        logger.info(String.format("%-" + (longName + 1) + "s %-" + (longOwner + 1) + "s %-" + longCreated + "s",
                                "Name", "Owner", "Created"));
                        logger.info(
                                String.format("%-" + longName + "s", " ").replace(' ', '-') + "  " +
                                        String.format("%-" + longOwner + "s", " ").replace(' ', '-') + "  " +
                                        String.format("%-" + longCreated + "s", " ").replace(' ', '-'));
                        for (Bucket bucket : buckets)
                            logger.info(String.format("%-" + (longName + 1) + "s %-" + (longOwner + 1) + "s %-" + longCreated + "s",
                                    bucket.getName(), bucket.getOwner().getDisplayName(), bucket.getCreationDate().toString()));
                    } else
                        logger.info("You have no buckets.");
                }
                break;
            case "upload":
                if (directories.size() > 2) {
                    if (!bagit.equals("none") || !compression.equals("none")) {
                        String toUpload = Encapsulation.encapsulate(directories.get(1), bagit, hashing, hiddenFiles, compression);
                        if (os.upload(directories.get(0), toUpload, directories.get(2), remove)) {
                            String results = "[" + directories.get(1) + "] has been";
                            if (!bagit.equals("none"))
                                results += " bagged up and";
                            if (!compression.equals("none"))
                                results += " compressed and";
                            results += " uploaded to [" + directories.get(0) + "] with prefix [" + directories.get(2) + "].";
                            logger.info(results);
                        } else {
                            logger.error("Failed to upload.");
                        }
                        if (!bagit.equals("none") && compression.equals("none"))
                            Encapsulation.debagify(directories.get(1));
                        if (!compression.equals("none") && (toUpload.endsWith(".tar") || toUpload.endsWith(".tar.bz2") ||
                                toUpload.endsWith(".tar.gz") || toUpload.endsWith(".tar.xz") ||
                                toUpload.endsWith(".zip")) && !save)
                            Files.delete(new File(toUpload).toPath());
                    } else {
                        if (os.upload(directories.get(0), directories.get(1), directories.get(2), remove))
                            logger.info("[{}] has been uploaded to [{}] with prefix [{}].", directories.get(1),
                                    directories.get(0), directories.get(2));
                        else
                            logger.error("Failed to upload.");
                    }
                } else if (directories.size() > 1) {
                    if (!bagit.equals("none") || !compression.equals("none")) {
                        String toUpload = Encapsulation.encapsulate(directories.get(1), bagit, hashing, hiddenFiles, compression);
                        if (os.upload(directories.get(0), toUpload, "", remove)) {
                            String results = "[" + directories.get(1) + "] has been";
                            if (!bagit.equals("none"))
                                results += " bagged up and";
                            if (!compression.equals("none"))
                                results += " compressed and";
                            results += " uploaded to [" + directories.get(0) + "].";
                            logger.info(results);
                        } else
                            logger.error("Failed to upload.");
                        if (!bagit.equals("none") && compression.equals("none"))
                            Encapsulation.debagify(directories.get(1));
                        if (!compression.equals("none") && (toUpload.endsWith(".tar") || toUpload.endsWith(".tar.bz2") ||
                                toUpload.endsWith(".tar.gz") || toUpload.endsWith(".tar.xz") ||
                                toUpload.endsWith(".zip")) && !save)
                            Files.delete(new File(toUpload).toPath());
                    } else {
                        if (os.upload(directories.get(0), directories.get(1), "", remove))
                            logger.info("[{}] has been uploaded to [{}].", directories.get(0),
                                    directories.get(1));
                        else
                            logger.error("Failed to upload.");
                    }
                } else
                    logger.error("Upload usage: bucket to_upload [object_prefix]");
                break;
            case "download":
                if (directories.size() > 2) {
                    os.download(directories.get(0), directories.get(1), directories.get(2));
                    logger.info("Objects from [{}] matching [{}] have been downloaded to [{}].", directories.get(0),
                            directories.get(1), directories.get(2));
                    String source = directories.get(0);
                    if (!source.endsWith("/"))
                        source += "/";
                    source += directories.get(1);
                    String output = directories.get(2);
                    if (!output.endsWith("/"))
                        output += "/";
                    output += directories.get(1);
                    String result = Encapsulation.restore(output);
                    if (result == null) {
                        logger.error("Failed to restore [{}]", source);
                        new File(output).delete();
                    } else
                        logger.info("[{}] has been restored to [{}]", source, result);
                } else
                    logger.error("Download usage: bucket to_download local_path");
                break;
            case "delete":
                if (directories.size() > 0) {
                    if (os.doesBucketExist(directories.get(0))) {
                        if (directories.size() > 1) {
                            if (os.doesObjectExist(directories.get(0), directories.get(1))) {
                                os.deleteObject(directories.get(0), directories.get(1));
                                logger.info("[{}] has been deleted from bucket [{}].", directories.get(1), directories.get(0));
                            } else {
                                List<S3ObjectSummary> objects = os.bucketContentsWithPrefex(directories.get(0), directories.get(1));
                                if (objects.size() > 0) {
                                    for (S3ObjectSummary object : objects)
                                        os.deleteObject(directories.get(0), object.getKey());
                                    logger.info("[{}] has been cleared of all items with the prefix [{}]", directories.get(0), directories.get(1));
                                } else
                                    logger.error("Bucket [{}] does not contain object [{}].", directories.get(0), directories.get(1));
                            }
                        } else {
                            List<S3ObjectSummary> objects = os.bucketContents(directories.get(0));
                            if (objects.size() < 1) {
                                os.deleteBucket(directories.get(0));
                                logger.info("Bucket [{}] has been deleted.", directories.get(0));
                            } else {
                                System.out.print("Bucket is not empty. Delete all files and bucket [Y/n]? ");
                                Scanner scanner = new Scanner(System.in);
                                String response = scanner.nextLine().toLowerCase().trim();
                                while (!response.equals("y") && !response.equals("n")) {
                                    System.out.print("Please enter 'y' or 'n': ");
                                    response = scanner.nextLine().toLowerCase().trim();
                                }
                                if (response.equals("y")) {
                                    for (S3ObjectSummary object : objects) {
                                        os.deleteObject(directories.get(0), object.getKey());
                                    }
                                    os.deleteBucket(directories.get(0));
                                } else {
                                    logger.info("You must remove all objects from a bucket before it may be deleted.");
                                }
                            }
                        }
                    } else
                        logger.error("Bucket [{}] does not exist.", directories.get(0));

                } else
                    logger.error("You must supply a bucket name.");
                break;
            case "pack":
                if (directories.size() < 1) {
                    logger.error("Compress usage: dir_to_compress [output_file]");
                    return;
                }
                File dirToPack = new File(directories.get(0));
                File packFile = new File(dirToPack.getAbsolutePath() + ".tar");
                if (directories.size() == 2)
                    packFile = new File(directories.get(1));
                Encapsulation.pack(packFile, dirToPack);
                if (packFile.exists() && packFile.isFile())
                    logger.info("Packed [{}] -> [{}]", dirToPack.getAbsolutePath(), packFile.getAbsolutePath());
                else
                    logger.error("Compression failed");
                break;
            case "compress":
                if (directories.size() < 1) {
                    logger.error("Compress usage: dir_to_compress [output_file]");
                    return;
                }
                File dirToCompress = new File(directories.get(0));
                File compressFile = new File(dirToCompress.getAbsolutePath() + ".tar.gz");
                if (directories.size() == 2)
                    compressFile = new File(directories.get(1));
                Encapsulation.compress(compressFile, dirToCompress);
                if (compressFile.exists() && compressFile.isFile())
                    logger.info("Compressed [{}] -> [{}]", dirToCompress.getAbsolutePath(), compressFile.getAbsolutePath());
                else
                    logger.error("Compression failed");
                break;
            case "unpack":
                if (directories.size() < 2) {
                    logger.error("Unpack usage: .tar_file target_dir");
                    return;
                }
                File packedFile = new File(directories.get(0));
                File unpackDir = new File(directories.get(1));
                logger.info("Unpacking [{}] -> [{}]", packedFile.getAbsolutePath(), unpackDir.getAbsolutePath());
                if (Encapsulation.unpack(packedFile, unpackDir)) {
                    logger.info("Successfully unpacked");
                } else {
                    logger.error("Unpacking failed");
                }
                break;
            case "decompress":
                if (directories.size() < 2) {
                    logger.error("Decompress usage: .tar.gz_file target_dir");
                    return;
                }
                File compressedFile = new File(directories.get(0));
                File decompressDir = new File(directories.get(1));
                logger.info("Decompressing [{}] -> [{}]", compressedFile.getAbsolutePath(), decompressDir.getAbsolutePath());
                if (Encapsulation.decompress(compressedFile, decompressDir)) {
                    logger.info("Successfully decompressed");
                } else {
                    logger.error("Decompressing failed");
                }
                break;
            case "test":
                //logger.info("[{}] {} a BagIt directory", directories.get(0), (Encapsulation.isBag(directories.get(0))) ? "is" : "is not");
                Encapsulation.unarchive(directories.get(0), directories.get(1));
                break;
        }
    }
}
