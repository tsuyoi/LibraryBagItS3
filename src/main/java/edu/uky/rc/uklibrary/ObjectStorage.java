package edu.uky.rc.uklibrary;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.*;
import com.amazonaws.services.s3.transfer.model.UploadResult;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.ion.NullValueException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static com.amazonaws.services.s3.internal.Constants.MAXIMUM_UPLOAD_PARTS;

public class ObjectStorage {
    private static final int MAX_NUMBER_OF_CONNECTIONS = 50;
    private static final Logger logger = LoggerFactory.getLogger(ObjectStorage.class);

    private AmazonS3 conn;

    /**
     * Constructor (Builds the S3 connection instance)
     * @param accessKey Your S3 access key (provided by the S3 service)
     * @param secretKey Your S3 secret key (provided by the S3 service)
     * @param endpoint The URL by which you access the S3 service
     */
    public ObjectStorage(String accessKey, String secretKey, String endpoint) throws NullValueException {
        this(accessKey, secretKey, endpoint, MAX_NUMBER_OF_CONNECTIONS);
    }

    /**
     * Constructor (Builds the S3 connection instance)
     * @param accessKey Your S3 access key (provided by the S3 service)
     * @param secretKey Your S3 secret key (provided by the S3 service)
     * @param endpoint The URL by which you access the S3 service
     * @param maxConnections The maximum number of connections to use during transfers
     */
    public ObjectStorage(String accessKey, String secretKey, String endpoint, int maxConnections) {
        this(accessKey, secretKey, endpoint, maxConnections, "");
    }

    /**
     * Constructor (Builds the S3 connection instance)
     * @param accessKey Your S3 access key (provided by the S3 service)
     * @param secretKey Your S3 secret key (provided by the S3 service)
     * @param endpoint The URL by which you access the S3 service
     * @param maxConnections The maximum number of connections to use during transfers
     * @param region The specific region at the given endpoint
     */
    public ObjectStorage(String accessKey, String secretKey, String endpoint, int maxConnections, String region) {
        logger.debug("Call to ObjectStorage constructor(...)");
        if (accessKey == null || accessKey.equals("")) {
            throw new NullValueException("accessKey cannot be empty");
        }
        if (secretKey == null || secretKey.equals("")) {
            throw new NullValueException("secretKey cannot be empty");
        }
        if (endpoint == null || endpoint.equals("")) {
            throw new NullValueException("endpoint cannot be empty");
        }
        if (region == null) {
            throw new NullValueException("region cannot be null");
        }
        logger.trace("Building credentials");
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        logger.trace("Building S3 client configuration");
        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setProtocol(Protocol.HTTPS);
        clientConfiguration.setSignerOverride("S3SignerType");
        clientConfiguration.setMaxConnections(maxConnections);
        logger.trace("Building S3 client");
        conn = AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(new EndpointConfiguration(endpoint, region))
                .withClientConfiguration(clientConfiguration)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withAccelerateModeEnabled(false)
                .withPathStyleAccessEnabled(true)
                .withPayloadSigningEnabled(false)
                .build();
        logger.trace("S3 client initialized");
    }

    /**
     * Function to list the buckets (top level prefixes) for the given account
     * @return A List of Bucket objects
     */
    public List<Bucket> buckets() {
        logger.debug("Call to buckets()");
        return conn.listBuckets();
    }

    /**
     * Method to check that a bucket exists
     * @param bucket Name of the bucket
     * @return Whether the bucket exists for this user
     */
    public boolean doesBucketExist(String bucket) {
        logger.debug("Call to doesBucketExist({})", bucket);
        return conn.doesBucketExistV2(bucket);
    }

    public boolean doesObjectExist(String bucket, String s3Key) {
        logger.debug("Call to doesObjectExist({}, {})", bucket, s3Key);
        return conn.doesObjectExist(bucket, s3Key);
    }

    /**
     * Method to create a new bucket for this account
     * @param bucket Name of the bucket
     * @return Whether the bucket was created
     */
    public boolean createBucket(String bucket) {
        logger.debug("Call to createBucket({})", bucket);
        return (conn.createBucket(bucket) != null);
    }

    /**
     * Method to delete a bucket
     * @param bucket Name of the bucket
     */
    public void deleteBucket(String bucket) {
        logger.debug("Call to deleteBucket({})", bucket);
        conn.deleteBucket(bucket);
    }

    /**
     * Method to delete an object
     * @param bucket Name of the containing bucket
     * @param key Key of the object
     */
    public void deleteObject(String bucket, String key) {
        logger.debug("Call to deleteObject({}, {})", bucket, key);
        conn.deleteObject(bucket, key);
    }

    /**
     * Method to view the objects inside a bucket
     * @param bucket Name of the bucket
     * @return A list of the S3ObjectSummary objects inside the given bucket
     */
    public List<S3ObjectSummary> bucketContents(String bucket) {
        logger.debug("Call to bucketContents({})", bucket);
        return conn.listObjects(bucket).getObjectSummaries();
    }

    public List<S3ObjectSummary> bucketContentsWithPrefex(String bucket, String prefix) {
        logger.debug("Call to bucketContentsWithPrefix({})", bucket, prefix);
        return conn.listObjects(bucket, prefix).getObjectSummaries();
    }

    /**
     * Method to upload an object to the object store
     * @param bucket Bucket to upload to
     * @param toUpload Path of the object(s) to upload
     * @param s3Prefix Prefix to append to the upload
     * @param removeExisting Whether to remove existing object(s)
     * @return Whether the upload was successful
     */
    public boolean upload(String bucket, String toUpload, String s3Prefix, boolean removeExisting) {
        if (bucket == null || bucket.equals("") || !doesBucketExist(bucket)) {
            logger.error("You must supply a valid bucket name.");
            return false;
        }
        if (toUpload == null || toUpload.equals("") || !(new File(toUpload).exists())) {
            logger.error("You must supply something to upload.");
            return false;
        }
        if (s3Prefix == null)
            s3Prefix = "";
        if (!s3Prefix.equals("") && !s3Prefix.endsWith("/"))
            s3Prefix += "/";
        File toUploadFile = new File(toUpload);
        if (toUploadFile.isFile())
            return uploadFile(bucket, toUpload, s3Prefix, removeExisting);
        else
            return uploadDirectory(bucket, toUpload, s3Prefix);
    }

    /**
     * Function to upload an entire directory to S3 with a given prefix (parent directory structure)
     * @param bucket The name of the bucket (top level prefix)
     * @param inFile The file to upload
     * @param s3Prefix The prefix (inside the bucket) at which to upload the files
     * @param removeExisting Whether to remove existing object
     * @return Whether the upload was successful
     */
    public boolean uploadFile(String bucket, String inFile, String s3Prefix, boolean removeExisting) {
        logger.debug("Call to uploadFile(bucket={}, inFile={}, s3Prefix={})", bucket, inFile, s3Prefix);
        boolean success = false;
        TransferManager manager = null;
        logger.trace("Building TransferManager");
        try {
            manager = TransferManagerBuilder.standard()
                    .withS3Client(conn)
                    .withMultipartUploadThreshold(1024L * 1024L * 5L)
                    .withMinimumUploadPartSize(1024L * 1024L * 5L)
                    .build();
            logger.trace("Checking that inDirectory exists");
            File uploadFile = new File(inFile);
            if (!uploadFile.exists()) {
                logger.error("Input directory [{}] does not exist!");
                return false;
            }
            inFile = Paths.get(inFile).toString();
            logger.trace("New inFile: {}", inFile);
            logger.trace("file.seperator: {}", File.separatorChar);
            s3Prefix += inFile.substring((inFile.lastIndexOf(File.separatorChar) > -1 ?
                    inFile.lastIndexOf(File.separatorChar) + 1 : 0));
            logger.trace("s3Prefix: {}", s3Prefix);
            if (conn.doesObjectExist(bucket, s3Prefix)) {
                logger.trace("[{}] already contains the object [{}]", bucket, s3Prefix);
                S3Object existingObject = conn.getObject(bucket, s3Prefix);
                String s3PrefixRename = s3Prefix + "." + new SimpleDateFormat("yyyy-MM-dd.HH-mm-ss-SSS")
                        .format(existingObject.getObjectMetadata().getLastModified());
                if (!removeExisting) {
                    logger.trace("[{}/{}] being renamed to [{}/{}]", bucket, s3Prefix, bucket, s3PrefixRename);
                    if (existingObject.getObjectMetadata().getContentLength() > 5L * 1024L * 1024L) {
                        InitiateMultipartUploadRequest initRequest = new InitiateMultipartUploadRequest(bucket,
                                s3PrefixRename);
                        InitiateMultipartUploadResult initResult = conn.initiateMultipartUpload(initRequest);
                        long objectSize = existingObject.getObjectMetadata().getContentLength();
                        long partSize = 5 * 1024 * 1024;
                        long bytePosition = 0;
                        int partNum = 1;
                        int numParts = (int)(objectSize / partSize);
                        int notificationStep = 5;
                        int percentDone = 0;
                        int nextPercent = percentDone + notificationStep;
                        List<CopyPartResult> copyResponses = new ArrayList<>();
                        logger.trace("Starting multipart upload ({} parts) to copy [{}/{}] to [{}/{}]", numParts,
                                bucket, s3Prefix, bucket, s3PrefixRename);
                        while (bytePosition < objectSize) {
                            if ((partNum/numParts) > nextPercent) {
                                logger.trace("Copying in progress ({}/{} {}%)", partNum, numParts, (partNum/numParts));
                                nextPercent = percentDone + notificationStep;
                            }
                            long lastByte = Math.min(bytePosition + partSize - 1, objectSize - 1);
                            CopyPartRequest copyRequest = new CopyPartRequest()
                                    .withSourceBucketName(bucket)
                                    .withSourceKey(s3Prefix)
                                    .withDestinationBucketName(bucket)
                                    .withDestinationKey(s3PrefixRename)
                                    .withUploadId(initResult.getUploadId())
                                    .withFirstByte(bytePosition)
                                    .withLastByte(lastByte)
                                    .withPartNumber(partNum++);
                            copyResponses.add(conn.copyPart(copyRequest));
                            bytePosition += partSize;
                        }
                        logger.trace("Creating multipart upload completion request");
                        CompleteMultipartUploadRequest completeRequest = new CompleteMultipartUploadRequest(
                                bucket, s3PrefixRename, initResult.getUploadId(), getETags(copyResponses)
                        );
                        logger.trace("Completing multipart upload");
                        conn.completeMultipartUpload(completeRequest);
                    } else {
                        CopyObjectRequest copyObjectRequest = new CopyObjectRequest(bucket, s3Prefix, bucket,
                                s3PrefixRename);
                        conn.copyObject(copyObjectRequest);
                    }
                }
                logger.trace("Deleting existing object [{}/{}]", bucket, s3Prefix);
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(bucket, s3Prefix);
                conn.deleteObject(deleteObjectRequest);
                logger.trace("[{}] is ready for upload of [{}]", bucket, s3Prefix);
            }
            PutObjectRequest request = new PutObjectRequest(bucket, s3Prefix, uploadFile);
            request.setGeneralProgressListener(new LoggingProgressListener(uploadFile.length()));
            logger.trace("Building upload montior and starting upload");
            long uploadStartTime = System.currentTimeMillis();
            Upload transfer = manager.upload(request);
            UploadResult result = transfer.waitForUploadResult();
            long uploadEndTime = System.currentTimeMillis();
            Duration uploadDuration = Duration.of(uploadEndTime - uploadStartTime, ChronoUnit.MILLIS);
            logger.trace("Upload finished in {}", formatDuration(uploadDuration));
            String s3Checksum = result.getETag();
            logger.trace("s3Checksum: {}", result.getETag());
            MD5Tools md5Tools = new MD5Tools(manager);
            String localChecksum;
            if (s3Checksum.contains("-"))
                localChecksum = md5Tools.getMultiCheckSum(inFile);
            else
                localChecksum = md5Tools.getCheckSum(inFile);
            logger.trace("localChecksum: {}", localChecksum);
            if (!localChecksum.equals(result.getETag()))
                logger.error("Checksums don't match [local: {}, S3: {}]", localChecksum, result.getETag());
            success = localChecksum.equals(result.getETag());
        } catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
        } catch (SdkClientException ace) {
            logger.error("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());
        } catch (InterruptedException ie) {
            logger.error("Interrupted error");
        } catch (IOException ioe) {
            logger.error("IOException error: {}", ioe.getMessage());
        } finally {
            try {
                assert manager != null;
                manager.shutdownNow();
            } catch (AssertionError ae) {
                logger.error("uploadFile : TransferManager was pre-emptively shut down.");
                return false;
            }
        }
        return success;
    }

    /**
     * Function to upload an entire directory to S3 with a given prefix
     * @param bucket The name of the bucket
     * @param inDirectory The local directory to upload
     * @param s3Prefix The prefix at which to upload the files
     * @return Whether the upload was successful
     */
    public boolean uploadDirectory(String bucket, String inDirectory, String s3Prefix) {
        logger.debug("Call to uploadDirectory(bucket={}, inDirectory={}, s3Prefix={})", bucket, inDirectory, s3Prefix);
        boolean success = false;
        TransferManager manager = null;
        logger.trace("Building TransferManager");
        try {
            manager = TransferManagerBuilder.standard()
                    .withS3Client(conn)
                    .withMultipartUploadThreshold(1024L * 1024L * 5L)
                    .withMinimumUploadPartSize(1024L * 1024L * 5L)
                    .build();
            logger.trace("Checking that inDirectory exists");
            File uploadDir = new File(inDirectory);
            if (!uploadDir.exists()) {
                logger.error("Input directory [{}] does not exist!");
                return false;
            }
            logger.trace("Capturing start time for transfer metrics");
            long startUpload = System.currentTimeMillis();
            logger.trace("Building upload montior and starting upload");
            MultipleFileUpload transfer = manager.uploadDirectory(bucket, s3Prefix, uploadDir, true);
            DecimalFormat percentFormatter = new DecimalFormat("#.##");
            logger.debug("Transfer: {}", transfer.getDescription());
            while (!transfer.isDone()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    logger.error("Upload interrupted!");
                }
                float transferTime = (System.currentTimeMillis() - startUpload) / 1000;
                long bytesTransfered = transfer.getProgress().getBytesTransferred();
                float transferRate = (bytesTransfered / 1000000) / transferTime;
                logger.debug("\tState: {}", transfer.getState());
                logger.debug("\tSub-Transfers: {}", transfer.getSubTransfers().size());
                logger.debug("\tTransferred: {}/{} bytes ({}%)", transfer.getProgress().getBytesTransferred(), transfer.getProgress().getTotalBytesToTransfer(), percentFormatter.format(transfer.getProgress().getPercentTransferred()));
                logger.debug("\tElapsed Time: {}", (transferTime < 60 ? ((int) transferTime + " seconds") : ((int) (transferTime / 60) + " minutes " + ((transferTime % 60 > 0) ? transferTime % 60 + " seconds" : ""))));
                logger.debug("\tTransfer Rate: {} MB/s", transferRate);
                for (int i = 0; i < 4; i++) {
                    if (!transfer.isDone()) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            logger.error("Upload interrupted!");
                            return false;
                        }
                    }
                }
            }
            success = true;
        } catch (Exception e) {
            logger.error("uploadDirectory : {}", e.getMessage());
        } finally {
            try {
                assert manager != null;
                manager.shutdownNow();
            } catch (AssertionError e) {
                logger.error("uploadDirectory : TransferManager was pre-emptively shut down.");
                return false;
            }
        }
        return success;
    }

    /**
     * Method to download from the object store
     * @param bucket Bucket to download from
     * @param s3 Object or prefix to download
     * @param outputDirectory Local directory to download into
     * @return
     */
    public boolean download(String bucket, String s3, String outputDirectory) {
        logger.debug("Call to download({}, {}, {})", bucket, s3, outputDirectory);
        if (bucket == null || bucket.equals("") || !doesBucketExist(bucket)) {
            logger.error("You must supply a valid bucket name.");
            return false;
        }
        if (s3 == null) {
            logger.error("You supply something to upload.");
            return false;
        }
        if (!outputDirectory.endsWith("/"))
            outputDirectory += "/";
        logger.trace("Checking that outputDirectory exists");
        File downloadDirectory = new File(outputDirectory);
        if (!downloadDirectory.exists()) {
            if (!downloadDirectory.mkdirs()) {
                logger.error("Output directory [{}] does not exist and could not be created!", downloadDirectory.getAbsolutePath());
                return false;
            }
        }
        if (doesObjectExist(bucket, s3))
            return downloadFile(bucket, s3, outputDirectory);
        else
            return downloadDirectory(bucket, s3, outputDirectory);
    }

    /**
     * Method to download a file from S3 to a local directory
     * @param bucket The name of the bucket
     * @param s3Key The key of the object to download
     * @param outputDirectory Where to download the file
     * @return Whether the download was successful
     */
    public boolean downloadFile(String bucket, String s3Key, String outputDirectory) {
        logger.debug("Call to downloadFile({}, {}, {})", bucket, s3Key, outputDirectory);
        boolean success = false;
        TransferManager manager = null;
        logger.trace("Building TransferManager");
        try {
            if (!conn.doesObjectExist(bucket, s3Key)) {
                logger.error("[{}] does not contain the object [{}]", bucket, s3Key);
                return false;
            }
            S3Object s3Object = conn.getObject(bucket, s3Key);
            String s3Checksum = s3Object.getObjectMetadata().getETag();
            logger.debug("s3Checksum: {}", s3Checksum);
            manager = TransferManagerBuilder.standard()
                    .withS3Client(conn)
                    .withMultipartUploadThreshold(1024L * 1024L * 5L)
                    .withMinimumUploadPartSize(1024L * 1024L * 5L)
                    .build();
            String outFileName = outputDirectory + s3Key.substring(s3Key.lastIndexOf("/") > -1 ? s3Key.lastIndexOf("/") : 0);
            logger.debug("outFile {}", outFileName);
            File outFile = new File(outFileName);
            GetObjectRequest request = new GetObjectRequest(bucket, s3Key);
            request.setGeneralProgressListener(new LoggingProgressListener(s3Object.getObjectMetadata().getContentLength()));
            logger.trace("Building download monitor and starting download");
            Download transfer = manager.download(request, outFile);
            transfer.waitForCompletion();
            if (!outFile.exists()) {
                logger.error("[{}] does not exist after download of [{}]", outFile.getAbsolutePath(), bucket + "/" + s3Key);
                return false;
            }
            MD5Tools md5Tools = new MD5Tools(manager);
            String localChecksum;
            if (s3Checksum.contains("-"))
                localChecksum = md5Tools.getMultiCheckSum(outFile.getAbsolutePath());
            else
                localChecksum = md5Tools.getCheckSum(outFile.getAbsolutePath());
            logger.trace("localChecksum: {}", localChecksum);
            if (!localChecksum.equals(s3Checksum))
                logger.error("Checksums don't match [local: {}, S3: {}]", localChecksum, s3Checksum);
            success = localChecksum.equals(s3Checksum);
        } catch (AmazonServiceException ase) {
            logger.error("Caught an AmazonServiceException, which means your request made it "
                    + "to Amazon S3, but was rejected with an error response for some reason.");
            logger.error("Error Message:    " + ase.getMessage());
            logger.error("HTTP Status Code: " + ase.getStatusCode());
            logger.error("AWS Error Code:   " + ase.getErrorCode());
            logger.error("Error Type:       " + ase.getErrorType());
            logger.error("Request ID:       " + ase.getRequestId());
        } catch (SdkClientException ace) {
            logger.error("Caught an AmazonClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with S3, "
                    + "such as not being able to access the network.");
            logger.error("Error Message: " + ace.getMessage());
        } catch (InterruptedException ie) {
            logger.error("Interrupted error");
        } catch (IOException ioe) {
            logger.error("IOException error: {}", ioe.getMessage());
        } finally {
            try {
                assert manager != null;
                manager.shutdownNow();
            } catch (AssertionError e) {
                logger.error("downloadFile : TransferManager was pre-emptively shut down.");
                return false;
            }
        }
        return success;
    }

    /**
     * Method to download an entire prefix from S3 to a local directory
     * @param bucket The name of the bucket
     * @param s3Prefix The prefix to download
     * @param outputDirectory Where to download the file
     * @return Whether the download was successful
     */
    public boolean downloadDirectory(String bucket, String s3Prefix, String outputDirectory) {
        logger.debug("Call to downloadDirectory({}, {}, {})", bucket, s3Prefix, outputDirectory);
        boolean success = false;
        TransferManager manager = null;
        logger.trace("Building TransferManager");
        try {
            manager = TransferManagerBuilder.standard()
                    .withS3Client(conn)
                    .withMultipartUploadThreshold(1024L * 1024L * 5L)
                    .withMinimumUploadPartSize(1024L * 1024L * 5L)
                    .build();
            logger.trace("Capturing start time for transfer metrics");
            long startUpload = System.currentTimeMillis();
            logger.trace("Building download monitor and starting download");
            MultipleFileDownload transfer = manager.downloadDirectory(bucket, s3Prefix, new File(outputDirectory));
            //Download transfer = manager.download(request, outFile);
            DecimalFormat percentFormatter = new DecimalFormat("#.##");
            logger.debug("Transfer: {}", transfer.getDescription());
            while (!transfer.isDone()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    logger.error("Download interrupted!");
                }
                float transferTime = (System.currentTimeMillis() - startUpload) / 1000;
                long bytesTransfered = transfer.getProgress().getBytesTransferred();
                float transferRate = (bytesTransfered / 1000000) / transferTime;
                logger.debug("\tState: {}", transfer.getState());
                logger.debug("\tTransferred: {}/{} bytes ({}%)", transfer.getProgress().getBytesTransferred(), transfer.getProgress().getTotalBytesToTransfer(), percentFormatter.format(transfer.getProgress().getPercentTransferred()));
                logger.debug("\tElapsed Time: {}", (transferTime < 60 ? ((int) transferTime + " seconds") : ((int) (transferTime / 60) + " minutes " + ((transferTime % 60 > 0) ? transferTime % 60 + " seconds" : ""))));
                logger.debug("\tTransfer Rate: {} MB/s\n", transferRate);
                for (int i = 0; i < 4; i++) {
                    if (!transfer.isDone()) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            logger.error("Download interrupted!");
                            return false;
                        }
                    }
                }
            }
            success = true;
        } catch (Exception e) {
            logger.error("downloadFile : {}", e.getMessage());
        } finally {
            try {
                assert manager != null;
                manager.shutdownNow();
            } catch (AssertionError e) {
                logger.error("downloadDirectory : TransferManager was pre-emptively shut down.");
                return false;
            }
        }
        return success;
    }

    /*
     *              HELPER METHODS
     *              These methods are helper methods for the public methods of this class
     */

    private class LoggingProgressListener implements ProgressListener {
        private final Logger logger = LoggerFactory.getLogger(LoggingProgressListener.class);
        private int updatePercentStep = 5;
        private long lastTimestamp;
        private long totalTransferred = 0L;
        private long lastTransferred = 0L;
        private long totalBytes;
        private int nextUpdate = updatePercentStep;

        public LoggingProgressListener(long totalBytes) {
            this.lastTimestamp = System.currentTimeMillis();
            this.totalBytes = totalBytes;
        }

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            Thread.currentThread().setName("TransferListener");
            long currentBytesTransferred = progressEvent.getBytesTransferred();
            this.totalTransferred += currentBytesTransferred;
            this.lastTransferred += currentBytesTransferred;
            float currentTransferPercentage = ((float)totalTransferred / (float)totalBytes) * (float)100;
            if (currentTransferPercentage > (float)nextUpdate) {
                long currentTimestamp = System.currentTimeMillis();
                logger.info("Transferred in progress ({}/{} {}%) at {}",
                        humanReadableByteCount(totalTransferred, true), humanReadableByteCount(totalBytes, true),
                        (int)currentTransferPercentage,
                        humanReadableTransferRate(lastTransferred, currentTimestamp - lastTimestamp));
                lastTransferred = 0L;
                lastTimestamp = currentTimestamp;
                nextUpdate += updatePercentStep;
            }
        }
    }

    /**
     * Formats the given bytes count in a human-readable way
     * @param bytes The number of bytes
     * @param si Whether to use the IEC standard notation
     * @return The formatted string
     */
    private static String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }

    private static String humanReadableTransferRate(long transferred, long duration) {
        float rate = (((float)transferred * (float)1000) * (float)8) / duration;
        int unit = 1000;
        if ((int)rate < unit) return String.format("%.1f bps", rate);
        int exp = (int) (Math.log(rate) / Math.log(unit));
        String pre = "kMGTPE".charAt(exp - 1) + "";
        return String.format("%.1f %sbps", rate / Math.pow(unit, exp), pre);
    }

    private static String formatDuration(Duration duration) {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        String positive = String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
        return seconds < 0 ? "-" + positive : positive;
    }

    // This is a helper function to construct a list of ETags.
    private static List<PartETag> getETags(List<CopyPartResult> responses) {
        List<PartETag> etags = new ArrayList<>();
        for (CopyPartResult response : responses) {
            etags.add(new PartETag(response.getPartNumber(), response.getETag()));
        }
        return etags;
    }

    private class MD5Tools {
        private final Logger logger = LoggerFactory.getLogger(MD5Tools.class);
        private final TransferManager manager;

        MD5Tools(TransferManager manager) {
            this.manager = manager;
        }

        String getMultiCheckSum(String fileName) throws IOException {
            logger.debug("Call to getMultiCheckSum [filename = {}]", fileName);
            String mpHash = null;
            FileInputStream fis = null;
            List<String> hashList = new ArrayList<>();

            try {
                MessageDigest md = MessageDigest.getInstance("MD5");

                File inputFile = new File(fileName);
                long partSize = calculateOptimalPartSize(inputFile.length(), manager.getConfiguration());
                fis = new FileInputStream(inputFile);
                boolean isReading = true;
                long bytesRead = 0;
                while (isReading) {
                    byte[] bs;

                    long remaining = inputFile.length() - bytesRead;
                    //System.out.println("remaingin: " + remaining);
                    if (remaining > partSize) {
                        bs = new byte[(int)partSize];
                        bytesRead = bytesRead + fis.read(bs, 0, (int)partSize);
                    } else {
                        bs = new byte[(int) remaining];
                        bytesRead = bytesRead + fis.read(bs, 0, (int)remaining);
                    }
                    byte[] hash = md.digest(bs);
                    hashList.add(getMD5(hash));
                    if (bytesRead == inputFile.length()) {
                        isReading = false;
                    }
                }
                mpHash = calculateChecksumForMultipartUpload(hashList);
            } catch (IOException ioe) {
                // Blah
            } catch (Exception ex) {
                System.out.println("MD5Tools : getMultiPartHash Error " + ex.toString());
            } finally {
                try {
                    assert fis != null;
                    fis.close();
                } catch (AssertionError ae) {
                    logger.error("getMultiCheckSum FileInputStream closed prematurely");
                } catch (IOException ioe) {
                    logger.error("getMultiCheckSum : fis.close() (IO) {}", ioe.getMessage());
                } catch (Exception e) {
                    logger.error("getMultiCheckSum : fis.close() {}", e.getMessage());
                }
            }
            return mpHash;
        }

        private String calculateChecksumForMultipartUpload(List<String> md5s) {
            StringBuilder stringBuilder = new StringBuilder();
            for (String md5 : md5s) {
                stringBuilder.append(md5);
            }

            String hex = stringBuilder.toString();
            byte raw[] = BaseEncoding.base16().decode(hex.toUpperCase());
            Hasher hasher = Hashing.md5().newHasher();
            hasher.putBytes(raw);
            String digest = hasher.hash().toString();

            return digest + "-" + md5s.size();
        }

        private String getMD52(byte[] hash) {
            Hasher hasher = Hashing.md5().newHasher();
            hasher.putBytes(hash);
            return hasher.hash().toString();
        }

        private String getMD5(byte[] hash) {
            StringBuilder hexString = new StringBuilder();
            for (byte hashByte : hash) {
                //for (int i = 0; i < hash.length; i++) {
                if ((0xff & /*hash[i]*/ hashByte) < 0x10) {
                    hexString.append("0");
                    hexString.append(Integer.toHexString(0xFF & /*hash[i]*/ hashByte));
                } else {
                    hexString.append(Integer.toHexString(0xFF & /*hash[i]*/ hashByte));
                }
            }
            return hexString.toString();
        }

        String getCheckSum(String path) throws IOException {
            logger.debug("Call to getCheckSum [path = {}]", path);
            String hash = null;
            //FileInputStream fis = null;
            try (FileInputStream fis = new FileInputStream(new File(path))) {
                //fis = new FileInputStream(new File(path));
                hash = org.apache.commons.codec.digest.DigestUtils.md5Hex(fis);
            } catch (Exception ex) {
                System.out.println("MD5Tools : getCheckSum Error : " + ex.toString());
            }/* finally {
            fis.close();
		}*/
            return hash;

        }

        public String getCheckSum2(String path) {
            String checksum = null;
            try {
                FileInputStream fis = new FileInputStream(path);
                MessageDigest md = MessageDigest.getInstance("MD5");

                //Using MessageDigest update() method to provide input
                byte[] buffer = new byte[8192];
                long numOfBytesRead;
                while ((numOfBytesRead = fis.read(buffer)) > 0) {
                    md.update(buffer, 0, (int) numOfBytesRead);
                }
                byte[] hash = md.digest();
                checksum = new BigInteger(1, hash).toString(16); //don't use this, truncates leading zero
                fis.close();
            } catch (Exception ex) {
                System.out.println("ObjectEngine : checkSum");
            }
            return checksum;
        }

        private long calculateOptimalPartSize(long contentLength, TransferManagerConfiguration configuration) {
            double optimalPartSize = (double)contentLength / (double)MAXIMUM_UPLOAD_PARTS;
            // round up so we don't push the upload over the maximum number of parts
            optimalPartSize = Math.ceil(optimalPartSize);
            return (long)Math.max(optimalPartSize, configuration.getMinimumUploadPartSize());
        }
    }
}
