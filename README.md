# Library S3/BagIt Proof-of-Concept

### [Information](information)
This project is a proof-of-concept (PoC) for the included S3 Java class ([ObjectStorage.java](https://gitlab.rc.uky.edu/cdhick2/Library-BagIt-S3/blob/master/src/main/java/edu/uky/rc/uklibrary/ObjectStorage.java)) file included in this project. The included [Wrapper.java](https://gitlab.rc.uky.edu/cdhick2/Library-BagIt-S3/blob/master/src/main/java/edu/uky/rc/uklibrary/Wrapper.java) class demonstrates how to utilize the S3 class to connect to an S3-compatible storage system.

### [Requirements](requirements)
* Java 8
* Maven 3+

### [To Build](build)
To build this project, simply clone this repository and issue the maven command: `mvn clean package`

### [To Use](usage)
_To be written_